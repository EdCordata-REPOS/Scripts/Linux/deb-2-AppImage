#!/bin/bash


NAME=vivaldi-6.4.3160.44
ORIGINAL_LAUNCHER=vivaldi


# Clean
# -----------------------------------------------------------
rm -rf ./app.appdir;
rm -rf ./tmp;
# -----------------------------------------------------------


echo "* Extracting deb..."
# -----------------------------------------------------------
mkdir -p ./tmp;
ar x --output ./tmp ./*.deb;
tar -xf ./tmp/data.tar.xz --directory=./tmp;
# -----------------------------------------------------------


echo "* Preparing files..."
# -----------------------------------------------------------
mkdir -p ./app.appdir;
bash -c "cd ./tmp/opt/* && mv ./* ./../../../app.appdir";
mv "./app.appdir/$ORIGINAL_LAUNCHER" ./app.appdir/AppRun;

rm -rf ./app.appdir/*.desktop;
rm -rf ./app.appdir/icon.svg;
rm -rf ./app.appdir/icon.png;

cp -rf ./launcher.desktop "./app.appdir/$NAME.desktop";

if [ -e ./icon.svg ]; then
    cp -rf ./icon.svg "./app.appdir/icon.svg";
fi

if [ -e ./icon.png ]; then
    cp -rf ./icon.png "./app.appdir/icon.png";
fi
# -----------------------------------------------------------


echo "* Prepare App Image Tool...";
# -----------------------------------------------------------
if [ ! -f "./appimagetool-x86_64.AppImage" ]; then
  wget https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage;
fi

chmod +x ./appimagetool-x86_64.AppImage;
# -----------------------------------------------------------


echo "* Building App...";
# -----------------------------------------------------------
./appimagetool-x86_64.AppImage --comp gzip ./app.appdir -n;
# -----------------------------------------------------------


# Clean
# -----------------------------------------------------------
rm -rf ./app.appdir;
rm -rf ./tmp
# -----------------------------------------------------------


echo "";
echo "Done.";
