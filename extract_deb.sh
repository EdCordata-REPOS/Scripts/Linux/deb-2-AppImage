#!/bin/bash


rm -rf ./deb;
mkdir -p ./deb;
ar x --output ./deb ./*.deb;
tar -xf ./deb/data.tar.xz --directory=./deb;

echo "";
echo "Done.";
