# Deb 2 AppImage

* copy deb file to current folder (name doesn't matter)
* setup `icon.svg` or `icon.png` icon
* setup `Name` and `Exec` in `launcher.desktop`
* setup `Category` in `launcher.desktop` (https://askubuntu.com/a/674411)
* update `ORIGINAL_LAUNCHER` and `NAME` variables in `build_from_deb.sh`
* run `build_from_deb.sh`
